"use strict";

/* global app, socket, bootbox */

(function () {
  require(["composer", "alerts", "hooks"], function (composer, alerts, hooks) {
    $(window).on("action:composer.post.edit", function (event, params) {
      socket.emit(
        "plugins.connectedittimeout.checkEditable",
        { pid: params.pid },
        function (err, errMessage) {
          if (err) {
            setTimeout(function () {
              composer.discard(composer.active);
            }, 500);
            alerts.alert(errMessage);
          }
        }
      );
    });

    hooks.on("filter:composer.error", (data) => {
      if (data.message == "timeout.skip_alert") {
        data.showAlert = false;
        composer.discard(data.post_uuid);
      }
      return data;
    });

    socket.on("connectedittimeout:composerclose", function (params) {
      alerts.alert(params);
    });
  });
})();
