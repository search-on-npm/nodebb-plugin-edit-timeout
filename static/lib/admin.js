"use strict";
/* globals $, app, socket */

define("admin/plugins/connect-edit-timeout", ["settings", "alerts"], function (
  Settings,
  alerts
) {
  var ACP = {};

  ACP.init = function () {
    Settings.load("connect-edit-timeout", $(".connect-edit-timeout-settings"));

    $("#save").on("click", function () {
      var valoreTempo = parseInt($("#timeout").val());

      if (valoreTempo > 0) {
        Settings.save(
          "connect-edit-timeout",
          $(".connect-edit-timeout-settings"),
          function () {
            alerts.alert({
              type: "success",
              alert_id: "connect-edit-timeout-saved",
              title: "Settings Saved",
              message: "Please reload your NodeBB to apply these settings",
              clickfn: function () {
                socket.emit("admin.reload");
              },
            });
          }
        );
      } else {
        alerts.alert({
          type: "danger",
          alert_id: "connect-edit-timeout-error",
          title: "Wrong Input",
          message: "Put only integer value",
        });
      }
    });

    $("#suggest-list [data-pid]").each(function () {
      var pid = $(this).data("pid");
      var el = $(this);

      $(this)
        .find(".dismiss")
        .click(function () {
          bootbox.dialog({
            title: "Dismiss?",
            message: "Dismiss the edit?",
            buttons: {
              success: {
                label: "Dismiss",
                className: "btn-warning",
                callback: function () {
                  socket.emit(
                    "plugins.connectedittimeout.dismiss",
                    { pid: pid },
                    function (err, returnData) {
                      if (err) {
                        alerts.error(returnData.message);
                      } else {
                        el.fadeOut(500, function () {
                          alerts.success(returnData.message);
                        });
                      }
                    }
                  );
                },
              },
            },
          });
        });

      $(this)
        .find(".approve")
        .click(function () {
          bootbox.dialog({
            title: "Approve?",
            message: "Approve the edit?",
            buttons: {
              success: {
                label: "Approve",
                className: "btn-success",
                callback: function () {
                  socket.emit(
                    "plugins.connectedittimeout.approve",
                    { pid: pid },
                    function (err, returnData) {
                      if (err) {
                        alerts.error(returnData.message);
                      } else {
                        el.fadeOut(500, function () {
                          alerts.success(returnData.message);
                        });
                      }
                    }
                  );
                },
              },
            },
          });
        });
    });
  };

  return ACP;
});
