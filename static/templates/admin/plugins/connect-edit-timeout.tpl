<div class="row">
  <ul class="nav nav-pills">
    <li class="active">
      <a href="#settings" data-toggle="tab"
        >[[connect_edit_timeout:admin-settings-title]]</a
      >
    </li>
    <li>
      <a href="#suggest-list" data-toggle="tab"
        >[[connect_edit_timeout:admin-to-approve]]</a
      >
    </li>
  </ul>
  <br />
  <div class="tab-content">
    <div class="tab-pane fade active in row" id="settings">
      <form role="form" class="connect-edit-timeout-settings">
        <div class="col-sm-2 col-xs-12 settings-header">
          [[connect_edit_timeout:admin-settings-title]]
        </div>
        <div class="col-sm-10 col-xs-12">
          <p class="lead">
            [[connect_edit_timeout:admin-settings-description]]
          </p>
          <div class="form-group">
            <label for="timeout">[[connect_edit_timeout:admin-timeout]]</label>
            <input
              type="text"
              id="timeout"
              name="timeout"
              title="Tineout"
              class="form-control"
              placeholder="Timeout"
            /><br />
          </div>
        </div>
      </form>
      <button
        id="save"
        class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
      >
        <i class="material-icons">save</i>
      </button>
    </div>
    <div class="tab-pane fade in row" id="suggest-list">
      <div class="post-container" data-next="{next}">
        <!-- IF !posts.length -->
        [[connect_edit_timeout:nothing-to-approve]]
        <!-- ENDIF !posts.length -->

        <!-- BEGIN posts -->
        <div
          class="panel panel-default"
          data-pid="{posts.pid}"
          data-tid="{posts.topic.tid}"
        >
          <div class="panel-body">
            <a href="{config.relative_path}/user/{posts.user.userslug}">
              <img
                title="{posts.user.username}"
                class="img-rounded user-img"
                src="{posts.user.picture}"
              />
            </a>

            <a href="{config.relative_path}/user/{posts.user.userslug}">
              <strong><span>{posts.user.username}</span></strong>
            </a>
            <div class="content">
              <p>{posts.content}</p>
              <p class="fade-out"></p>
            </div>
            <div class="clearfix">
              <small>
                <span class="pull-right">
                  Edited
                  <span
                    class="timeago"
                    title="{posts.relativeTimeSuggest}"
                  ></span>
                  &bull; Posted in
                  <a
                    href="{config.relative_path}/category/{posts.category.slug}"
                    target="_blank"
                    ><i class="fa {posts.category.icon}"></i>
                    {posts.category.name}</a
                  >,
                  <span class="timeago" title="{posts.relativeTime}"></span>
                  &bull;
                  <a
                    href="{config.relative_path}/topic/{posts.topic.slug}/{posts.index}"
                    target="_blank"
                    >Read More</a
                  >
                </span>
              </small>
            </div>
            <div>
              <hr />
              <button class="btn btn-warning dismiss">
                [[connect_edit_timeout:dismiss]]
              </button>
              <button class="btn btn-success approve pull-right">
                [[connect_edit_timeout:approve]]
              </button>
            </div>
          </div>
        </div>
        <!-- END posts -->
      </div>
    </div>
  </div>
</div>
