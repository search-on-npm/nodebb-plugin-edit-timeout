"use strict";

var sockets = require("./lib/sockets"),
  controllers = require("./lib/controllers"),
  meta = require.main.require("./src/meta"),
  async = require.main.require("async"),
  User = require.main.require("./src/user"),
  topics = require.main.require("./src/topics"),
  posts = require.main.require("./src/posts"),
  websockets = require.main.require("./src/socket.io"),
  db = require.main.require("./src/database"),
  notifications = require.main.require("./src/notifications"),
  winston = require.main.require("winston"),
  SocketPlugins = require.main.require("./src/socket.io/plugins"),
  plugin = {};
SocketPlugins.connectedittimeout = sockets;
const routeHelpers = require.main.require("./src/routes/helpers");

plugin.init = async (params) => {
  routeHelpers.setupAdminPageRoute(
    params.router,
    "/admin/plugins/connect-edit-timeout",
    [],
    controllers.renderAdminPage
  );
};

plugin.addAdminNavigation = async (header) => {
  header.plugins.push({
    route: "/plugins/connect-edit-timeout",
    icon: "fa-tint",
    name: "Edit Timeout",
  });

  return header;
};

/**
 * Save history post
 */
plugin.postEdited = async (data) => {
  const settingsQueue = await meta.settings.get("connect-edit-timeout");
  let pidh = 1;
  if (settingsQueue && settingsQueue.counter) {
    pidh = parseInt(settingsQueue.counter) + 1;
  }
  await db.sortedSetAdd(
    "connect-edit-timeout:pid:" + data.pid + ":history:post",
    data.edited,
    pidh
  );
  await db.setObject(
    "connect-edit-timeout:pid:" + data.pid + ":history:pidh:" + pidh,
    data
  );
  await meta.settings.set("connect-edit-timeout", { counter: pidh });
};

/**
 * Check if post is editable. If the maximum time has elapsed editing is saved in the suggestion
 */
plugin.checkIfEditable = async (data) => {
  var now = Date.now();
  var timeout = 0;
  const isAdmin = await User.isAdministrator(data.uid);
  const cid = await topics.getTopicField(data.post.tid, "cid");

  const isModerator = await User.isModerator(data.uid, cid);
  const postData = await posts.getPostData(data.data.pid);

  const settingTimeout = await meta.settings.get("connect-edit-timeout");

  if (
    settingTimeout &&
    settingTimeout.timeout &&
    !isNaN(settingTimeout.timeout)
  ) {
    timeout =
      new Date().getTime() - parseInt(settingTimeout.timeout) * 60 * 1000;
  }

  const edited = await db.getObject(
    "connect-edit-timeout:" + data.data.pid + ":suggest"
  );
  if (edited) {
    websockets.in("uid_" + data.uid).emit("connectedittimeout:composerclose", {
      title: "[[connect_edit_timeout:already-suggest-title]]",
      message: "[[connect_edit_timeout:already-suggest-message]]",
      type: "warning",
      timeout: 10000,
    });
    throw new Error("timeout.skip_alert");
  }

  if (isAdmin || isModerator || postData.timestamp > timeout) {
    return data;
  } else {
    await db.setObject(
      "connect-edit-timeout:" + data.data.pid + ":suggest",
      data.post
    );
    await db.sortedSetAdd("connect-edit-timeout:suggest", now, data.data.pid);

    websockets.in("uid_" + data.uid).emit("connectedittimeout:composerclose", {
      title: "[[connect_edit_timeout:thanks-suggest-title]]",
      message: "[[connect_edit_timeout:thanks-suggest-message]]",
      type: "info",
      timeout: 10000,
    });
    const notif = await notifications.create({
      bodyShort: "[[connect_edit_timeout:new-suggest-notification]]",
      nid:
        "plugins:connect-edit-timeout:" +
        data.data.pid +
        ":uid:" +
        data.uid +
        ":suggest:" +
        new Date().getTime(),
      from: data.uid,
      path: "/admin/plugins/connect-edit-timeout#suggest-list",
      importance: 6,
    });
    notifications.pushGroup(notif, "administrators");
    throw new Error("timeout.skip_alert");
  }
};

plugin.test = async (data) => {
  console.log(data);
};

plugin.checkIfDeletable = function (data, callback) {
  winston.verbose("[connect-edit-timeout] checkIfDeletable hook");
  winston.verbose("[connect-edit-timeout] Data: " + JSON.stringify(data));
  async.parallel(
    {
      isAdmin: function (next) {
        User.isAdministrator(data.uid, next);
      },
      isModerator: function (next) {
        posts.getPostField(data.pid, "tid", function (err, post_tid) {
          topics.getTopicField(post_tid, "cid", function (err, post_cid) {
            User.isModerator(data.uid, post_cid, next);
          });
        });
      },
    },
    function (err, results) {
      if (err) {
        winston.verbose("[connect-edit-timeout] Error: " + JSON.stringify(err));
        return callback(err, data);
      }

      // Se non amministratore o non moderatore => errore
      if (!results.isAdmin && !results.isModerator) {
        winston.verbose("[connect-edit-timeout] !isAdmin && !isModerator ");
        return callback(new Error("Non è possibile eliminare un post"), data);
      }
      callback(null, data);
    }
  );
};

module.exports = plugin;
