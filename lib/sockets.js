"use strict";

var db = require.main.require("./src/database"),
  winston = require.main.require("winston"),
  async = require.main.require("async"),
  utils = require.main.require("./public/src/utils"),
  plugins = require.main.require("./src/plugins"),
  Notifications = require.main.require("./src/notifications"),
  Posts = require.main.require("./src/posts"),
  topics = require.main.require("./src/topics"),
  meta = require.main.require("./src/meta"),
  User = require.main.require("./src/user");

var Sockets = {};

Sockets.checkEditable = function (socket, data, callback) {
  db.getObject(
    "connect-edit-timeout:" + data.pid + ":suggest",
    function (err, postSuggest) {
      if (err) {
        return callback(err);
      }
      if (postSuggest) {
        return callback(true, {
          title: "[[connect_edit_timeout:already-suggest-title]]",
          message: "[[connect_edit_timeout:already-suggest-message]]",
          type: "warning",
          timeout: 10000,
        });
      }
      callback();
    }
  );
};

Sockets.dismiss = function (socket, data, callback) {
  // Verifico se l'oggetto è ancora presente
  db.getObject(
    "connect-edit-timeout:" + data.pid + ":suggest",
    function (err, postSuggest) {
      if (err) {
        winston.error(
          "[connect-edit-timeout] [Sockets.dismiss.err1] " + JSON.stringify(err)
        );
        return callback(err, {
          message: "Si è verificato un errore grave",
        });
      }
      // Se esiste
      if (postSuggest) {
        //Elimino Set
        db.sortedSetRemove(
          "connect-edit-timeout:suggest",
          data.pid,
          function (err) {
            if (err) {
              winston.error(
                "[connect-edit-timeout] [Sockets.dismiss.err2] " +
                  JSON.stringify(err)
              );
              return callback(err, {
                message: "Si è verificato un errore grave",
              });
            }
            // Elimino la modifica in suggest
            db.delete(
              "connect-edit-timeout:" + data.pid + ":suggest",
              function (err) {
                if (err) {
                  winston.error(
                    "[connect-edit-timeout] [Sockets.dismiss.err3] " +
                      JSON.stringify(err)
                  );
                  return callback(err, {
                    message: "Si è verificato un errore grave",
                  });
                }

                //console.log(data);

                var slug;
                var postData;

                async.waterfall(
                  [
                    function (next) {
                      Posts.getPostData(data.pid, next);
                    },
                    function (postData1, next) {
                      if (!postData1) {
                        return callback(new Error("Post Inesistente"));
                      }
                      postData = postData1;
                      topics.getTopicData(
                        postData.tid,
                        function (err, topicData) {
                          if (!topicData) {
                            return callback(new Error("Topic Inesistente"));
                          }
                          return next(null, topicData);
                        }
                      );
                    },
                    function (topicData, next) {
                      if (!topicData) {
                        return callback(new Error("Topic inesistente"));
                      }
                      slug = topicData.slug;
                      topics.getPids(topicData.tid, next);
                    },
                  ],
                  function (err, pids) {
                    if (err) {
                      return callback(err);
                    }
                    if (!postData) {
                      return callback(new Error("Post non esistente"));
                    }

                    var uid = postData.uid;
                    var indice = 0;
                    for (var i = 0; i < pids.length; i++) {
                      if (pids[i] == postData.pid) {
                        indice = i;
                        break;
                      }
                    }
                    Notifications.create(
                      {
                        bodyShort: "[[connect_edit_timeout:refuses-change]]",
                        //bodyLong: 'bbbbb',
                        nid: "connect-moderators:dismiss:uid:" + uid,
                        //pid: 1,
                        //tid: 1,
                        path: "/topic/" + slug + "/" + (indice + 1),
                        from: socket.uid,
                        importance: 6,
                      },
                      function (err, notification) {
                        if (err || !notification) {
                          return callback("Si è verificato un errore: 4788");
                        }
                        Notifications.push(notification, uid);
                        return callback(null, {
                          message: "Post dismissed",
                        });
                      }
                    );
                    /*return callback(null, {
							message: 'Post dismissed'
						});*/
                  }
                );
              }
            );
          }
        );

        // Se non esiste più
      } else {
        return callback(err, {
          message: "Post già elaborato",
        });
      }
    }
  );
};

Sockets.approve = function (socket, data, callback) {
  console.log(data);
  // Verifico se l'oggetto è ancora presente
  db.getObject(
    "connect-edit-timeout:" + data.pid + ":suggest",
    function (err, postSuggest) {
      if (err) {
        winston.error(
          "[connect-edit-timeout] [Sockets.approve.err1] " + JSON.stringify(err)
        );
        return callback(err, {
          message: "Si è verificato un errore grave - 1",
        });
      }
      // Se esiste
      if (postSuggest) {
        //Elimino Set
        db.sortedSetRemove(
          "connect-edit-timeout:suggest",
          data.pid,
          function (err) {
            if (err) {
              winston.error(
                "[connect-edit-timeout] [Sockets.approve.err2] " +
                  JSON.stringify(err)
              );
              return callback(err, {
                message: "Si è verificato un errore grave - 2",
              });
            }
            // Elimino la modifica in suggest
            db.delete(
              "connect-edit-timeout:" + data.pid + ":suggest",
              function (err) {
                if (err) {
                  winston.error(
                    "[connect-edit-timeout] [Sockets.approve.err3] " +
                      JSON.stringify(err)
                  );
                  return callback(err, {
                    message: "Si è verificato un errore grave - 3",
                  });
                }
                var slug;
                var postData;

                async.waterfall(
                  [
                    function (next) {
                      Posts.getPostData(data.pid, next);
                    },
                    function (postData1, next) {
                      if (!postData1) {
                        return callback(new Error("Post Inesistente"));
                      }
                      postData = postData1;
                      topics.getTopicData(
                        postData.tid,
                        function (err, topicData) {
                          if (!topicData) {
                            return callback(new Error("Topic Inesistente"));
                          }
                          return next(null, topicData);
                        }
                      );
                    },
                    function (topicData, next) {
                      if (!topicData) {
                        return callback(new Error("Topic inesistente"));
                      }
                      slug = topicData.slug;
                      topics.getPids(topicData.tid, next);
                    },
                  ],
                  function (err, pids) {
                    if (err) {
                      return callback(err);
                    }
                    if (!postData) {
                      return callback(new Error("Post non esistente"));
                    }

                    var uid = postData.uid;
                    var indice = 0;
                    for (var i = 0; i < pids.length; i++) {
                      if (pids[i] == postData.pid) {
                        indice = i;
                        break;
                      }
                    }
                    async.waterfall(
                      [
                        function (next) {
                          Notifications.create(
                            {
                              bodyShort:
                                "[[connect_edit_timeout:accept-change]]",
                              //bodyLong: 'bbbbb',
                              nid: "connect-moderators:accept:uid:" + uid,
                              //pid: 1,
                              //tid: 1,
                              path: "/topic/" + slug + "/" + (indice + 1),
                              from: socket.uid,
                              importance: 6,
                            },
                            function (err, notification) {
                              if (err || !notification) {
                                return callback(
                                  "Si è verificato un errore: 4788"
                                );
                              }
                              Notifications.push(notification, uid);
                              return next();
                            }
                          );
                        },
                        function (next) {
                          Posts.edit(
                            {
                              uid: socket.uid, //postSuggest.editor,, // Passo l'id di chi approva
                              handle: postSuggest.handle,
                              pid: data.pid,
                              content: postSuggest.content,
                            },
                            function (err, result) {
                              if (err) {
                                winston.error(
                                  "[connect-edit-timeout] [Sockets.approve.err4] " +
                                    JSON.stringify(err)
                                );
                                //TODO
                                //revert delete
                                return callback(err, {
                                  message:
                                    "Si è verificato un errore grave - 4",
                                });
                              }
                              return next();
                            }
                          );
                        },
                      ],
                      function (err) {
                        if (err) {
                          return callback(err);
                        }
                        return callback(null, {
                          message: "Post Approved",
                        });

                        /*return callback(null, {
								message: 'Post dismissed'
							});*/
                      }
                    );
                  }
                );
              }
            );
          }
        );

        // Se non esiste più
      } else {
        return callback(err, {
          message: "Post già elaborato",
        });
      }
    }
  );
};

module.exports = Sockets;
