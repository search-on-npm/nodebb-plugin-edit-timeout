"use strict";
var Posts = require.main.require("./src/posts");
var db = require.main.require("./src/database");
var async = require.main.require("async");
var utils = require.main.require("./public/src/utils");
//var colors = require('colors');
var jsdiff = require("diff");
var Controllers = {};

Controllers.renderAdminPage = async (req, res, next) => {
  var start = 0;
  var stop = 49;

  var set = "connect-edit-timeout:suggest";
  let posts = [];

  const pids = await db.getSortedSetRevRange(set, start, stop);
  if (!Array.isArray(pids) || !pids.length) {
    posts = [];
  } else {
    posts = await Posts.getPostSummaryByPids(pids, req.user.uid, {
      parse: false,
      stripTags: false,
      extraFields: ["flags"],
    });
  }

  for (let index in posts) {
    posts[index].relativeTime = utils.toISOString(posts[index].timestamp);
    posts[index].relativeEditTime =
      parseInt(posts[index].edited, 10) !== 0
        ? utils.toISOString(posts[index].edited)
        : "";
    let suggest = await db.getObject(
      "connect-edit-timeout:" + posts[index].pid + ":suggest"
    );
    if (suggest) {
      posts[index].relativeTimeSuggest = utils.toISOString(suggest.edited);
      var contentNew = "";
      var diff = jsdiff.diffWords(posts[index].content, suggest.content);
      diff.forEach(function (part) {
        // green for additions, red for deletions
        // grey for common parts
        var color = part.added ? "green" : part.removed ? "red" : "grey";
        contentNew +=
          '<span style="color:' + color + '">' + part.value + "</span>";
        //post.content = 'aaaaa ' + post.content + "|||||||" + postSuggest.content;
      });
      posts[index].content = contentNew;
    }
  }

  res.render("admin/plugins/connect-edit-timeout", {
    posts: posts,
    next: stop + 1,
  });
};

module.exports = Controllers;
