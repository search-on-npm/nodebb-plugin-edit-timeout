# Edit Timeout

[FUNZIONALITA']

Permette di specificare, lato admin, il tempo massimo entro cui un utente può modificare un post. Esempio:<br />


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-edit-timeout/raw/master/screenshot/editTimeout.png)
<br />

Un utente può modificare quindi il post entro il tempo inserito da admin. Una volta scaduto il tempo massimo non è più possibile modificare il post in senso tradizione, ma verrà inviato un "suggerimento di modifica" all'admin che deciderà se approvare il post o meno:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-edit-timeout/raw/master/screenshot/editPost.png)
<br />

Di seguito viene viualizzato come, nel pannello dell'admin, verranno visualizzati i "suggerimenti di modifica " per i post:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-edit-timeout/raw/master/screenshot/showPost.png)

Se il suggerimento verrà accettato il post sarà modifico altrimenti non verrà cambiato.

